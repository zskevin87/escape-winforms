﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape.Persistence
{
    public class EscapeDataException : Exception //this exception is thrown when datamanaging (loading game, saving game) went wrong.
    {
        public EscapeDataException() { }
    }
}
