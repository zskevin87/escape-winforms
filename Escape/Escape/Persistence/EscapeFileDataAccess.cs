﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape.Persistence
{
    public class EscapeFileDataAccess : IEscapeDataAccess //using the data managing interface
    {
        public async Task<EscapeTable> LoadAsync(String path) //loading method
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    //reads the size of the table first
                    String line = await reader.ReadLineAsync() ?? String.Empty;
                    Int32 tableSize = Int32.Parse(line);
                    EscapeTable table = new EscapeTable(tableSize);

                    //reads n lines (size of table) to get the positions of the characters
                    for (Int32 i = 0; i < tableSize; i++)
                    {
                        line = await reader.ReadLineAsync() ?? String.Empty;
                        String[] numbers = line.Split();

                        for (Int32 j = 0; j < tableSize; j++)
                        {
                            if (Int32.Parse(numbers[j]) == 1) //founding one means there is the player on that field
                            {
                                table.SetPlayer(i, j);
                            }
                            else if (Int32.Parse(numbers[j]) == 2) //founding two means there is an enemy on that field
                            {
                                table.SetEnemy(i, j);
                            }
                            else if (Int32.Parse(numbers[j]) == 3) //founding three means that there are the player and an enemy on the same field (enemy caught player)
                            {
                                table.SetPlayer(i, j);
                                table.SetEnemy(i, j);
                            }
                            //else it's zero, so the field is empty
                        }
                    }
                    //reads another n lines to get the positions of the mines
                    for (Int32 i = 0; i < tableSize; i++)
                    {
                        line = await reader.ReadLineAsync() ?? String.Empty;
                        String[] mines = line.Split();

                        for (Int32 j = 0; j < tableSize; j++)
                        {
                            if (mines[j] == "1") //founding one means that there is a mine on that field
                            {
                                table.SetMine(i, j);
                            }
                        }
                        //else it's zero, so the field is empty
                    }
                    line = await reader.ReadLineAsync() ?? String.Empty; //reading the next line tells the coordinates of the first enemy
                    Point enemy1 = new Point(int.Parse(line.Split()[0]), int.Parse(line.Split()[1]));
                    line = await reader.ReadLineAsync() ?? String.Empty; //reading the next line tells the coordinates of the second enemy
                    Point enemy2 = new Point(int.Parse(line.Split()[0]), int.Parse(line.Split()[1]));
                    line = await reader.ReadLineAsync() ?? String.Empty; //reading the next line tells the playtime when the game was saved
                    int time = int.Parse(line.Split()[0]);
                    //setting EscapeTable properties to these datas
                    table.SetEnemyCoordinate(1, enemy1);
                    table.SetEnemyCoordinate(2, enemy2);
                    table.SetLoadGameTime(time);
                    //returning the loaded table
                    return table;
                }
            }
            catch //if something went wrong, we throw a data exception
            {
                throw new EscapeDataException();
            }
        }

        public async Task SaveAsync(String path, EscapeTable table, int time) //saving game method
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.Write(table.Size); //writing out the table size
                    writer.WriteLine();
                    //writing _fieldValues in n lines (n is the size of the table)
                    for (int i = 0; i < table.Size; i++)
                    {
                        for (Int32 j = 0; j < table.Size; j++)
                        {
                            await writer.WriteAsync(table[i, j] + " ");
                        }
                        await writer.WriteLineAsync();
                    }
                    //writing _fieldMines in n lines: if _fieldMines[x,y] is true, we write 1, else we write 0
                    for (int i = 0; i < table.Size; i++)
                    {
                        for (Int32 j = 0; j < table.Size; j++)
                        {
                            await writer.WriteAsync((table.IsMine(i,j) ? "1" : "0") + " ");
                        }
                        await writer.WriteLineAsync();
                    }
                    //writing out the coordinates of the enemies and the playtime in three new lines
                    writer.WriteLine(table.Enemy1Coordinates.X + " " + table.Enemy1Coordinates.Y);
                    writer.WriteLine(table.Enemy2Coordinates.X + " " + table.Enemy2Coordinates.Y);
                    writer.WriteLine(time);
                }
            }
            catch //if something went wrong, we throw a new data exception
            {
                throw new EscapeDataException();
            }
        }
    }
}
