﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape.Model
{
    //eventargs of game advancing and game over events
    public class EscapeEventArgs : EventArgs // the args are the actual playtime and the bool which tells if we won or not
    {
        private Int32 _gameTime;
        private Boolean _isWon;

        public Int32 GameTime { get { return _gameTime; } }
        public Boolean IsWon { get { return _isWon; } }

        public EscapeEventArgs(Int32 _gameTime, Boolean _isWon)
        {
            this._gameTime = _gameTime;
            this._isWon = _isWon;
        }
    }
}
