﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape.Model
{
    //eventargs of stepping event
    public class EscapeStepEventArgs : EventArgs //the event args are the fields, where a character/mines were, and where they are now.
    {
        private Int32 _oldX;
        private Int32 _oldY;
        private Int32 _changedFieldX;
        private Int32 _changedFieldY;

        public Int32 oldX { get { return _oldX; } }
        public Int32 oldY { get { return _oldY; } }
        public Int32 changedX { get { return _changedFieldX; } }
        public Int32 changedY { get { return _changedFieldY; } }

        public EscapeStepEventArgs(Int32 x, Int32 y, Int32 newX, Int32 newY)
        {
            _oldX = x;
            _oldY = y;
            _changedFieldX = newX;
            _changedFieldY = newY;
        }
    }
}
