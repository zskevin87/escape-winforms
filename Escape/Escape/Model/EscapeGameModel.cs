﻿using Escape.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Timers;

namespace Escape.Model
{
    public enum TableSize { ELEVEN, FIFTEEN, TWENTYONE}
    public class EscapeGameModel : IDisposable
    {
        #region Constant Values
        private const Int32 MinesNum = 6; //the number of mines is 6 in any game rounds
        // player (1 piece) /enemy (2 pieces) numbers are built in the methods. 
        #endregion

        #region Fields
        private IEscapeDataAccess _dataAccess;
        private EscapeTable _table = null!;
        private TableSize _tableSize;
        private Int32 _gameTime;
        private System.Timers.Timer _timer;

        #endregion

        #region Properties
        public EscapeTable Table { get { return _table; } } //returns the table and its properties
        public Int32 GameTime { get { return _gameTime; } } //returns the playtime
        public TableSize tableSize { get { return _tableSize; } private set { _tableSize = value; } } //returns the size of the table
        public Boolean IsGameOver { get //true if the game is over.
            {
                return (_table.AreAllEnemiesInMines()
                    || _table.IsPlayerInAMine(_table.PlayerCoordinates.X, _table.PlayerCoordinates.Y)
                    || _table.HasAnyEnemiesCaughtPlayer());
            }
        }
        #endregion

        #region Constructors
        public EscapeGameModel(TableSize tableSize, IEscapeDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
            _tableSize = tableSize;

            switch (_tableSize) //creating a table with the appropriate size
            {
                case TableSize.ELEVEN:
                    _table = new EscapeTable(11);
                    break;

                case TableSize.FIFTEEN:
                    _table = new EscapeTable(15);
                    break;

                case TableSize.TWENTYONE:
                    _table = new EscapeTable(21);
                    break;

                default:
                    break;
            }
            _gameTime = 0; //setting the timer 0
            _timer = new System.Timers.Timer(); //making a timer which controls the enemystepping and shows the elapsed time
            _timer.Interval = 1000;
            _timer.Elapsed += new ElapsedEventHandler(Timer_Tick);
            _timer.AutoReset = true;
            _timer.Start();
        }

        private void Timer_Tick(Object? sender, ElapsedEventArgs e) //if the timer 'ticks', one enemy stepping will reveal
        {
            SteppingEnemy();
            OnGameAdvanced();
        }
        #endregion

        #region Timer methods 
        //making public methods to control the timer
        public void StartTimer() //starting timer
        {
            Boolean timerElapsing = _timer.Enabled;
            if (timerElapsing)
                return;
            _timer.Start();
        }

        public void StopTimer() //stopping timer
        {
            bool timerElapsing = _timer.Enabled;
            if (!timerElapsing)
                return;
            _timer.Stop();
        }

        public Boolean IsTimerEnabled() // does the timer elsapse?
        {
            return _timer.Enabled;
        }

        public void AdvanceGame()
        {
            OnGameAdvanced();
        }
        #endregion

        #region Events
        public event EventHandler<EscapeStepEventArgs>? Stepping;

        public event EventHandler<EscapeEventArgs>? GameAdvanced;

        public event EventHandler<EscapeEventArgs>? GameOver;
        #endregion
        
        #region Public methods

        public void NewGame()
        {
            //creates a new table with the appropriate size
            switch (_tableSize)
            {
                case TableSize.ELEVEN:
                    _table = new EscapeTable(11);
                    break;

                case TableSize.FIFTEEN:
                    _table = new EscapeTable(15);
                    break;

                case TableSize.TWENTYONE:
                    _table = new EscapeTable(21);
                    break;

                default:
                    break;
            }
            _gameTime = 0; //setting the timer
            _timer.Start();
            //spawning characters and mines on the appropriate fields
            SetPlayer_Preparation(_table.Size);
            SetEnemies_Prepatation(_table.Size);
            GenerateMines_Preparation(_table.Size, MinesNum);
        }

        public void Step(Point xy) //stepping with the player
        {
            if (IsGameOver) { return; } //if the game is over, we cannot move
            Point oldField = new Point(_table.PlayerCoordinates.X, _table.PlayerCoordinates.Y);
            try
            {

                _table.StepPlayer(_table.PlayerCoordinates.X, _table.PlayerCoordinates.Y,
                    _table.PlayerCoordinates.X + xy.X, _table.PlayerCoordinates.Y + xy.Y);

                OnStepping(oldField.X, oldField.Y, _table.PlayerCoordinates.X, _table.PlayerCoordinates.Y);
            }
            catch (Exception)
            {
                return; // if the player wants to step out of the table, we won't let it. Nothing will happen.
            }

            if (Table.AreAllEnemiesInMines())
            {
                OnGameOver(true); //we won
            }
            else if (_table.HasAnyEnemiesCaughtPlayer()
                || _table.IsPlayerInAMine(_table.PlayerCoordinates.X, _table.PlayerCoordinates.Y))
            {
                OnGameOver(false); //we lost
            }
        }

        public void SteppingEnemy() //prepares the steppings of the enemies
        {
            if (IsGameOver) { return; } // if the game is over, enemies can't move
            Point oldFields1 = _table.Enemy1Coordinates;
            Point oldFields2 = _table.Enemy2Coordinates;
            _table.StepEnemies();

            OnStepping(oldFields1.X, oldFields1.Y, _table.Enemy1Coordinates.X, _table.Enemy1Coordinates.Y);
            OnStepping(oldFields2.X, oldFields2.Y, _table.Enemy2Coordinates.X, _table.Enemy2Coordinates.Y);
            _gameTime++; //as enemies moves one field per a second, the gametime equals to the total stepping of each enemies
            if (_table.HasAnyEnemiesCaughtPlayer()
                || _table.IsPlayerInAMine(_table.PlayerCoordinates.X, _table.PlayerCoordinates.Y))
            {
                OnGameOver(false); //we lost
            }

            else if (Table.AreAllEnemiesInMines())
            {
                OnGameOver(true); //we won
            }
        }
        
        public async Task LoadGameAsync(String path) //loading game
        {
            if (_dataAccess == null)
                throw new InvalidOperationException("No data access provided.");

            EscapeTable t = await _dataAccess.LoadAsync(path);
            if (t.Size != _table.Size)
                throw new InvalidOperationException("Not same size!");

            _table = t;//await _dataAccess.LoadAsync(path); JAVÍTÁS
            switch (_table.Size)
            {
                case 11:
                    _tableSize = TableSize.ELEVEN;
                    break;
                case 15:
                    _tableSize = TableSize.FIFTEEN;
                    break;
                case 21:
                    _tableSize = TableSize.TWENTYONE;
                    break;
            }
            _gameTime = _table.LoadedGameTime;
        }

        public async Task SaveGameAsync(String path)
        {
            if (_dataAccess == null)
                throw new InvalidOperationException("No data access is provided.");

            await _dataAccess.SaveAsync(path, _table, _gameTime);
        }

        #endregion

        #region Private methods
        private void SetPlayer_Preparation(Int32 tableSize) //prepares player to spawn on the convient field
        {
            if (tableSize % 2 == 0)
                _table.SetPlayer(0, tableSize / 2 - 1);
            else
                _table.SetPlayer(0, tableSize / 2);
        }

        private void SetEnemies_Prepatation(Int32 tableSize) //prepares the enemy to spawn on the convient fields
        {
            //only provides to spawn two enemies at the two downer edges
            _table.SetEnemy(tableSize - 1, 0);
            _table.SetEnemyCoordinate(1, new Point(tableSize - 1, 0)); //first enemy's coordinates
            _table.SetEnemy(tableSize - 1, tableSize - 1);
            _table.SetEnemyCoordinate(2, new Point(tableSize - 1, tableSize - 1)); //second enemy's coordinates
        }

        private void GenerateMines_Preparation(Int32 tableSize, Int32 MinesNum) //uses EscapeTable.SetMine() method properly
        {
            Point point = new Point();
            Int32 count = 0;
            Random random = new Random();
            do
            {
                point.X = random.Next(tableSize);
                point.Y = random.Next(tableSize);

                if (_table.IsEmpty(point.X, point.Y))
                {
                    _table.SetMine(point.X, point.Y);
                    count++;
                }
            }
            while (MinesNum != count);

        }
        #endregion

        #region Private event methods 
        private void OnStepping(Int32 x, Int32 y, Int32 newX, Int32 newY) //the event of stepping (player, enemies)
        {
            Stepping?.Invoke(this, new EscapeStepEventArgs(x, y, newX, newY));
        }

        private void OnGameAdvanced() //the event of the time elapsing/ game advancing
        {
            GameAdvanced?.Invoke(this, new EscapeEventArgs(_gameTime, false));
        }

        private void OnGameOver(Boolean isWon) //the event of the game over
        {
            GameOver?.Invoke(this, new EscapeEventArgs(_gameTime, isWon));
        }
        #endregion

        #region Disposing methods
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (_timer != null)
                    {
                        _timer.Stop();
                        _timer.Dispose();
                    }
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
