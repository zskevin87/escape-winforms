using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Escape.Model;
using Escape.Persistence;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Button;

namespace Escape.WinForms.View
{
    public partial class GameForm : Form
    {
        #region Fields
        private EscapeGameModel _model = null!; //instancing dataAccess and model to the view
        private Button[,] _buttonGrid = null!; //buttongrid will be the view of the EscapeTable
        #endregion

        #region Constructor
        public GameForm()
        {
            InitializeComponent();

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;

            //the welcome page will jump up, where player can choose the size of the table
            _labelPausedGame.Hide();
            _pauseButton.Hide();
            _buttonStepDown.Hide();
            _buttonStepUp.Hide();
            _buttonStepLeft.Hide();
            _buttonStepRight.Hide();
            _menuNewGame.Enabled = false;
            _menuSizeChange.Enabled = false;
            _menuLoadGame.Enabled = false;
            _menuSaveGame.Enabled = false;
            _menuExitGame.Enabled = false;

            this.FormClosing += GameForm_FormClosing;
        }
        #endregion

        #region Choosing Size methods
        private void _size11_Click(object sender, EventArgs e) //makes an 11x11 table, sets a pause button and d-pad
        {
            //setting an appropriate sized window for the game, hiding welcome page
            ClientSize = new Size(700, 550);
            _welcomeLabel.Hide();
            _size11.Hide();
            _size15.Hide();
            _size21.Hide();
            _pauseButton.Show();
            _buttonStepDown.Show();
            _buttonStepUp.Show();
            _buttonStepLeft.Show();
            _buttonStepRight.Show();
            _menuExitGame.Enabled = true;

            //creating model to play


            _model = new EscapeGameModel(TableSize.ELEVEN, new EscapeFileDataAccess());
            _model.Stepping += new EventHandler<EscapeStepEventArgs>(Game_Stepping);
            _model.GameAdvanced += new EventHandler<EscapeEventArgs>(Game_GameAdvanced);
            _model.GameOver += new EventHandler<EscapeEventArgs>(Game_GameOver);

            //setting up the table
            GenerateTable();

            //start a new game
            _model.NewGame();
            SetupTable();
        }

        private void _size15_Click(object sender, EventArgs e) //makes an 15x15 table, sets a pause button and d-pad (same adjustments like in the previous method)
        {
            ClientSize = new Size(900, 700);
            _welcomeLabel.Hide();
            _size11.Hide();
            _size15.Hide();
            _size21.Hide();
            _pauseButton.Show();
            _buttonStepDown.Show();
            _buttonStepUp.Show();
            _buttonStepLeft.Show();
            _buttonStepRight.Show();
            _menuExitGame.Enabled = true;

            _model = new EscapeGameModel(TableSize.FIFTEEN, new EscapeFileDataAccess());
            _model.Stepping += new EventHandler<EscapeStepEventArgs>(Game_Stepping);
            _model.GameAdvanced += new EventHandler<EscapeEventArgs>(Game_GameAdvanced);
            _model.GameOver += new EventHandler<EscapeEventArgs>(Game_GameOver);

            GenerateTable();

            _model.NewGame();
            SetupTable();
        }

        private void _size21_Click(object sender, EventArgs e) //makes an 21x21 table, sets a pause button and d-pad (same adjustments like in the previous method)
        {
            ClientSize = new Size(1100, 950);
            _welcomeLabel.Hide();
            _size11.Hide();
            _size15.Hide();
            _size21.Hide();
            _pauseButton.Show();
            _buttonStepDown.Show();
            _buttonStepUp.Show();
            _buttonStepLeft.Show();
            _buttonStepRight.Show();
            _menuExitGame.Enabled = true;

            _model = new EscapeGameModel(TableSize.TWENTYONE, new EscapeFileDataAccess());
            _model.Stepping += new EventHandler<EscapeStepEventArgs>(Game_Stepping);
            _model.GameAdvanced += new EventHandler<EscapeEventArgs>(Game_GameAdvanced);
            _model.GameOver += new EventHandler<EscapeEventArgs>(Game_GameOver);

            GenerateTable();

            _model.NewGame();
            SetupTable();
        }
        #endregion

        #region Standard event methods
        private void Game_Stepping(object? sender, EscapeStepEventArgs e) //set colours of the fields
        {
            if (_model.Table.IsMine(e.changedX, e.changedY)) //mine is red with an X
            {
                _buttonGrid[e.oldX, e.oldY].Invoke(new Action(() => _buttonGrid[e.oldX, e.oldY].Text = String.Empty));
                _buttonGrid[e.oldX, e.oldY].BackColor = Color.White;
                _buttonGrid[e.changedX, e.changedY].BackColor = Color.Red;
                _buttonGrid[e.changedX, e.changedY].Invoke(new Action(() => _buttonGrid[e.changedX, e.changedY].Text = "X"));
            }
            else if (_model.Table.IsEnemy(e.changedX, e.changedY)) //enemies are black
            {
                _buttonGrid[e.oldX, e.oldY].BackColor = Color.White;
                _buttonGrid[e.changedX, e.changedY].BackColor = Color.Black;
            }
            else if (_model.Table.IsPlayer(e.changedX, e.changedY)) //player is yellow
            {
                _buttonGrid[e.oldX, e.oldY].BackColor = Color.White;
                _buttonGrid[e.changedX, e.changedY].BackColor = Color.Yellow;
            }
            else
            {
                _buttonGrid[e.oldX, e.oldY].BackColor = Color.White; //empty fields are white
                _buttonGrid[e.changedX, e.changedY].BackColor = Color.White;
            }
        }

        private void Game_GameAdvanced(object? sender, EscapeEventArgs e)
        {
            _toolLabelGameTime.Text = TimeSpan.FromSeconds(e.GameTime).ToString("g"); //showing the playtime
        }

        private void Game_GameOver(object? sender, EscapeEventArgs e) //checking the status of game, if it is over, we show who won
        {
            _model.StopTimer();

            if (e.IsWon)
            {
                MessageBox.Show("You Won!" + Environment.NewLine +
                                "Your playtime was " +
                                TimeSpan.FromSeconds(e.GameTime).ToString("g"), "Escape Game",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Asterisk);
            }
            else
            {
                MessageBox.Show("You Lost!",
                                "Escape Game",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Asterisk);
            }

            this.Invoke((Action)delegate //letting player to adjust settings
            {
                _menuNewGame.Enabled = true;
                _menuSizeChange.Enabled = true;
                _menuLoadGame.Enabled = true;
                _menuSaveGame.Enabled = true;

                _pauseButton.Enabled = false;
                _buttonStepDown.Enabled = false;
                _buttonStepUp.Enabled = false;
                _buttonStepLeft.Enabled = false;
                _buttonStepRight.Enabled = false;

                _labelPausedGame.Show();
                _labelPausedGame.Text = "Game is Over.";
            });
        }
        #endregion

        #region Menu event handlers
        private void _menuNewGame_Click(object sender, EventArgs e) //starting a new game
        {
            Boolean restartTimer = _model.IsTimerEnabled();
            if (_model.IsGameOver)
            {
                _menuNewGame.Enabled = false;
                _menuSizeChange.Enabled = false;
                _menuLoadGame.Enabled = false;
                _menuSaveGame.Enabled = false;

                _model.StopTimer();


                _model.NewGame();
                SetupTable();

                _pauseButton.Text = "PAUSE";
                _labelPausedGame.Hide();
                _pauseButton.Enabled = true;
                _buttonStepLeft.Enabled = true;
                _buttonStepRight.Enabled = true;
                _buttonStepUp.Enabled = true;
                _buttonStepDown.Enabled = true;
            } //showing a Y/N message if the player wants to start a new game during a game 
            else if (MessageBox.Show("Are you sure you want to start a new game?" + Environment.NewLine + "Unsaved datas will be lost.", "Escape Game", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _menuNewGame.Enabled = false;
                _menuLoadGame.Enabled = false;
                _menuSaveGame.Enabled = false;

                _model.StopTimer();


                _model.NewGame();
                SetupTable();

                _pauseButton.Text = "PAUSE";
                _labelPausedGame.Hide();
                _pauseButton.Enabled = true;
                _buttonStepLeft.Enabled = true;
                _buttonStepRight.Enabled = true;
                _buttonStepUp.Enabled = true;
                _buttonStepDown.Enabled = true;
            }
            else //if the player cancels, timer starts
            {
                if (restartTimer)
                    _model.StartTimer();
            }
        }

        private void _menuSizeChange_Click(object sender, EventArgs e) //throws back to the welcome page.
        {
            Boolean restartTimer = _model.IsTimerEnabled(); //showing a Y/N message if the player wants to start a new game during a game 
            if (_model.IsGameOver || MessageBox.Show("Are you sure you want to change table size?" + Environment.NewLine + "Unsaved datas will be lost.", "Escape Game", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                System.Diagnostics.Process.Start(Application.ExecutablePath);
                Application.Exit();
            }
            else //if the player cancels, timer starts
            {
                if (restartTimer)
                    _model.StartTimer();
            }
        }

        private async void _menuLoadGame_Click(object sender, EventArgs e) //loads the chosen game
        {
            int actualsize = _model.Table.Size;
            EscapeGameModel actualModel = _model;
            Boolean restartTimer = _model.IsTimerEnabled();
            _model.StopTimer();

            if (_openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    await _model.LoadGameAsync(_openFileDialog.FileName);
                }
                catch (EscapeDataException) //messagebox jumps up if the fileformat was not ok
                {
                    MessageBox.Show("Loading game was unsuccessful!" + Environment.NewLine + "Not appropriate path or fileformat!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                catch (InvalidOperationException) // the player wants to load a game whether size is not the same like the original table size.
                {
                    MessageBox.Show("Loading game was unsuccessful!" + Environment.NewLine + "Couldn't match the selected file's table size to the originally selected table size." + Environment.NewLine + "Go back to the main menu and select another size of the table.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    _menuNewGame.Enabled = false;
                    _menuSizeChange.Enabled = false;
                    _menuLoadGame.Enabled = false;
                    _menuSaveGame.Enabled = false;

                    _model.NewGame();
                    SetupTable();

                    _pauseButton.Text = "PAUSE";
                    _labelPausedGame.Hide();
                    _pauseButton.Enabled = true;
                    _buttonStepLeft.Enabled = true;
                    _buttonStepRight.Enabled = true;
                    _buttonStepUp.Enabled = true;
                    _buttonStepDown.Enabled = true;
                    return;
                }
                // if the loading was successful, we set up the loaded table
                SetupTable();
                _toolLabelGameTime.Text = TimeSpan.FromSeconds(_model.GameTime).ToString("g");

                _pauseButton.Text = "CONTINUE";
                _pauseButton.Enabled = true;
                if (_model.IsGameOver)
                {
                    _pauseButton.Enabled = false;
                    _labelPausedGame.Text = "Game is Over.";
                }
                    
            }

            if (restartTimer) //if the player cancels, timer starts
                _model.StartTimer();
        }

        private async void _menuSaveGame_Click(object sender, EventArgs e) //saves game
        {
            Boolean restartTimer = _model.IsTimerEnabled();
            _model.StopTimer();

            if (_saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    await _model.SaveGameAsync(_saveFileDialog.FileName);

                }
                catch (EscapeDataException) //messagebox jumps up if the saving was unsuccessful
                {
                    MessageBox.Show("Saving game was unsuccessful!" + Environment.NewLine + "Not appropriate path, or the library is readonly.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (restartTimer) //if the player cancels, timer starts
                _model.StartTimer();
        }

        private void _menuExitGame_Click(object sender, EventArgs e)
        {
            Boolean restartTimer = _model.IsTimerEnabled();
            _model.StopTimer();

            if (_model.IsGameOver) //if it is game over, it just closes, else a Y/N messagebox jumps up
            {
                Close();
            }
            else if (MessageBox.Show("Are you sure you want to leave?" + Environment.NewLine + "Unsaved datas will be lost.", "Escape Game", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Close();
            }
            else
            {
                if (restartTimer) //if the player cancels, timer starts
                    _model.StartTimer();
            }
        }

        private void _pauseButton_Click(object sender, EventArgs e) //a button which controls pausing the game, and continuing
        {
            Boolean gamePaused = _model.IsTimerEnabled();

            if (gamePaused) //the game is not paused, we set it paused. Timer stops, player/enemies don't step, adjusting functions turn available
            {
                _model.StopTimer();
                _menuNewGame.Enabled = true;
                _menuSizeChange.Enabled = true;
                _menuLoadGame.Enabled = true;
                _menuSaveGame.Enabled = true;

                _buttonStepLeft.Enabled = false;
                _buttonStepRight.Enabled = false;
                _buttonStepUp.Enabled = false;
                _buttonStepDown.Enabled = false;

                _labelPausedGame.Show();
                _pauseButton.Text = "CONTINUE";
            }
            else //the game is paused, we continue the game
            {
                _model.StartTimer();
                _menuNewGame.Enabled = false;
                _menuSizeChange.Enabled = false;
                _menuLoadGame.Enabled = false;
                _menuSaveGame.Enabled = false;

                _buttonStepLeft.Enabled = true;
                _buttonStepRight.Enabled = true;
                _buttonStepUp.Enabled = true;
                _buttonStepDown.Enabled = true;

                _labelPausedGame.Hide();
                _pauseButton.Text = "PAUSE";
            }
        }
        #endregion

        #region Private table setting methods
        private void GenerateTable() //generates the view of the table with buttongrid
        {
            _buttonGrid = new Button[_model.Table.Size, _model.Table.Size];
            for (Int32 i = 0; i < _model.Table.Size; i++)
                for (Int32 j = 0; j < _model.Table.Size; j++)
                {
                    _buttonGrid[i, j] = new Button();
                    _buttonGrid[i, j].Location = new Point(5 + 40 * j, 35 + 40 * i);
                    _buttonGrid[i, j].Size = new Size(40, 40);
                    _buttonGrid[i, j].Font = new Font(FontFamily.GenericSansSerif, 15, FontStyle.Bold);
                    _buttonGrid[i, j].Enabled = false;
                    _buttonGrid[i, j].TabIndex = 100 + i * _model.Table.Size + j;
                    _buttonGrid[i, j].FlatStyle = FlatStyle.Flat;


                    Controls.Add(_buttonGrid[i, j]);

                }

            //makes the window's size flexible to the game
            _buttonStepLeft.Location = new Point(_model.Table.Size * 40 + 50, 387);
            _buttonStepDown.Location = new Point(_buttonStepLeft.Location.X + 60, 387);
            _buttonStepRight.Location = new Point(_buttonStepLeft.Location.X + 120, 387);
            _buttonStepUp.Location = new Point(_buttonStepLeft.Location.X + 60, 327);
            _pauseButton.Location = new Point(_buttonStepLeft.Location.X, 200);
            _labelPausedGame.Location = new Point(_buttonStepLeft.Location.X, 150);
        }

        private void SetupTable() //sets the characters and mines colours properly as in Game_Stepping()
        {
            for (Int32 i = 0; i < _buttonGrid.GetLength(0); i++)
            {
                for (Int32 j = 0; j < _buttonGrid.GetLength(1); j++)
                {

                    if (_model.Table.IsEmpty(i, j))
                    {
                        _buttonGrid[i, j].Text = String.Empty;
                        _buttonGrid[i, j].BackColor = Color.White;
                    }
                    else if (_model.Table.IsEnemy(i, j))
                    {
                        _buttonGrid[i, j].BackColor = Color.Black;
                    }
                    else if (_model.Table.IsPlayer(i, j))
                    {
                        _buttonGrid[i, j].BackColor = Color.Yellow;
                    }
                    if (_model.Table.IsMine(i, j))
                    {
                        _buttonGrid[i, j].BackColor = Color.Red;
                        _buttonGrid[i, j].Text = "X";
                    }
                }
            }
            //shows 0 timer firstly
            _toolLabelGameTime.Text = "0:00:00";
            _labelPausedGame.Text = "Game is Paused.";
        }
        #endregion

        #region Controlling Step
        //these methods give the convenient coordinates to step the player (how much fields to step up/down, hwo much fields to step left/right)
        private void _buttonStepUp_Click(object? sender, EventArgs e) //stepping up one field with the player 
        {
            _model.Step(new Point(-1, 0));
        }
        private void _buttonStepDown_Click(object? sender, EventArgs e) //stepping down one field
        {
            _model.Step(new Point(1, 0));
        }


        private void _buttonStepRight_Click(object? sender, EventArgs e) //stepping right one field
        {
            _model.Step(new Point(0, 1));
        }

        private void _buttonStepLeft_Click(object? sender, EventArgs e) // stepping left one field
        {
            _model.Step(new Point(0, -1));
        }
        #endregion

        #region Disposing Timer event
        private void GameForm_FormClosing(object? sender, FormClosingEventArgs e)
        {
            if (_model != null)
                _model.Dispose();
        }
        #endregion
    }
}


