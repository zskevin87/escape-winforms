﻿namespace Escape.WinForms.View
{
    partial class GameForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            statusStrip1 = new StatusStrip();
            _toolLabelGameTime = new ToolStripStatusLabel();
            menuStrip1 = new MenuStrip();
            fileToolStripMenuItem = new ToolStripMenuItem();
            _menuNewGame = new ToolStripMenuItem();
            _menuSizeChange = new ToolStripMenuItem();
            _menuLoadGame = new ToolStripMenuItem();
            _menuSaveGame = new ToolStripMenuItem();
            _menuExitGame = new ToolStripMenuItem();
            _openFileDialog = new OpenFileDialog();
            _saveFileDialog = new SaveFileDialog();
            _pauseButton = new Button();
            _buttonStepRight = new Button();
            _buttonStepLeft = new Button();
            _buttonStepDown = new Button();
            _buttonStepUp = new Button();
            _welcomeLabel = new Label();
            _size11 = new Button();
            _size15 = new Button();
            _size21 = new Button();
            _labelPausedGame = new Label();
            statusStrip1.SuspendLayout();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { _toolLabelGameTime });
            statusStrip1.Location = new Point(0, 357);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(875, 22);
            statusStrip1.TabIndex = 0;
            statusStrip1.Text = "statusStrip1";
            // 
            // _toolLabelGameTime
            // 
            _toolLabelGameTime.Name = "_toolLabelGameTime";
            _toolLabelGameTime.Size = new Size(43, 17);
            _toolLabelGameTime.Text = "0:00:00";
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { fileToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(875, 24);
            menuStrip1.TabIndex = 1;
            menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _menuNewGame, _menuSizeChange, _menuLoadGame, _menuSaveGame, _menuExitGame });
            fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            fileToolStripMenuItem.Size = new Size(37, 20);
            fileToolStripMenuItem.Text = "File";
            // 
            // _menuNewGame
            // 
            _menuNewGame.Name = "_menuNewGame";
            _menuNewGame.Size = new Size(166, 22);
            _menuNewGame.Text = "New Game";
            _menuNewGame.Click += _menuNewGame_Click;
            // 
            // _menuSizeChange
            // 
            _menuSizeChange.Name = "_menuSizeChange";
            _menuSizeChange.Size = new Size(166, 22);
            _menuSizeChange.Text = "Change table size";
            _menuSizeChange.Click += _menuSizeChange_Click;
            // 
            // _menuLoadGame
            // 
            _menuLoadGame.Name = "_menuLoadGame";
            _menuLoadGame.Size = new Size(166, 22);
            _menuLoadGame.Text = "Load Game";
            _menuLoadGame.Click += _menuLoadGame_Click;
            // 
            // _menuSaveGame
            // 
            _menuSaveGame.Name = "_menuSaveGame";
            _menuSaveGame.Size = new Size(166, 22);
            _menuSaveGame.Text = "Save Game";
            _menuSaveGame.Click += _menuSaveGame_Click;
            // 
            // _menuExitGame
            // 
            _menuExitGame.Name = "_menuExitGame";
            _menuExitGame.Size = new Size(166, 22);
            _menuExitGame.Text = "Exit Game";
            _menuExitGame.Click += _menuExitGame_Click;
            // 
            // _openFileDialog
            // 
            _openFileDialog.FileName = "openFileDialog1";
            // 
            // _pauseButton
            // 
            _pauseButton.Font = new Font("Segoe UI", 14.25F, FontStyle.Bold, GraphicsUnit.Point);
            _pauseButton.Location = new Point(950, 247);
            _pauseButton.Name = "_pauseButton";
            _pauseButton.Size = new Size(187, 42);
            _pauseButton.TabIndex = 2;
            _pauseButton.Text = "PAUSE";
            _pauseButton.UseVisualStyleBackColor = true;
            _pauseButton.Click += _pauseButton_Click;
            // 
            // _buttonStepRight
            // 
            _buttonStepRight.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            _buttonStepRight.Location = new Point(1073, 381);
            _buttonStepRight.Name = "_buttonStepRight";
            _buttonStepRight.Size = new Size(61, 59);
            _buttonStepRight.TabIndex = 10;
            _buttonStepRight.Text = "→";
            _buttonStepRight.UseVisualStyleBackColor = true;
            _buttonStepRight.Click += _buttonStepRight_Click;
            // 
            // _buttonStepLeft
            // 
            _buttonStepLeft.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            _buttonStepLeft.Location = new Point(957, 381);
            _buttonStepLeft.Name = "_buttonStepLeft";
            _buttonStepLeft.Size = new Size(61, 59);
            _buttonStepLeft.TabIndex = 9;
            _buttonStepLeft.Text = "←";
            _buttonStepLeft.UseVisualStyleBackColor = true;
            _buttonStepLeft.Click += _buttonStepLeft_Click;
            // 
            // _buttonStepDown
            // 
            _buttonStepDown.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            _buttonStepDown.Location = new Point(1015, 381);
            _buttonStepDown.Name = "_buttonStepDown";
            _buttonStepDown.Size = new Size(61, 59);
            _buttonStepDown.TabIndex = 8;
            _buttonStepDown.Text = "↓";
            _buttonStepDown.UseVisualStyleBackColor = true;
            _buttonStepDown.Click += _buttonStepDown_Click;
            // 
            // _buttonStepUp
            // 
            _buttonStepUp.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            _buttonStepUp.Location = new Point(1015, 326);
            _buttonStepUp.Name = "_buttonStepUp";
            _buttonStepUp.Size = new Size(61, 58);
            _buttonStepUp.TabIndex = 7;
            _buttonStepUp.Text = "↑";
            _buttonStepUp.UseVisualStyleBackColor = true;
            _buttonStepUp.Click += _buttonStepUp_Click;
            // 
            // _welcomeLabel
            // 
            _welcomeLabel.AutoSize = true;
            _welcomeLabel.Font = new Font("Segoe UI", 21.75F, FontStyle.Bold, GraphicsUnit.Point);
            _welcomeLabel.Location = new Point(31, 131);
            _welcomeLabel.Name = "_welcomeLabel";
            _welcomeLabel.Size = new Size(824, 40);
            _welcomeLabel.TabIndex = 11;
            _welcomeLabel.Text = "Welcome to the Escape Game! Choose the size of the table!";
            // 
            // _size11
            // 
            _size11.Font = new Font("Segoe UI", 14.25F, FontStyle.Bold, GraphicsUnit.Point);
            _size11.Location = new Point(186, 201);
            _size11.Name = "_size11";
            _size11.Size = new Size(109, 58);
            _size11.TabIndex = 12;
            _size11.Text = "11 X 11";
            _size11.UseVisualStyleBackColor = true;
            _size11.Click += _size11_Click;
            // 
            // _size15
            // 
            _size15.Font = new Font("Segoe UI", 14.25F, FontStyle.Bold, GraphicsUnit.Point);
            _size15.Location = new Point(396, 201);
            _size15.Name = "_size15";
            _size15.Size = new Size(109, 58);
            _size15.TabIndex = 13;
            _size15.Text = "15 X 15";
            _size15.UseVisualStyleBackColor = true;
            _size15.Click += _size15_Click;
            // 
            // _size21
            // 
            _size21.Font = new Font("Segoe UI", 14.25F, FontStyle.Bold, GraphicsUnit.Point);
            _size21.Location = new Point(587, 201);
            _size21.Name = "_size21";
            _size21.Size = new Size(109, 58);
            _size21.TabIndex = 14;
            _size21.Text = "21 X 21";
            _size21.UseVisualStyleBackColor = true;
            _size21.Click += _size21_Click;
            // 
            // _labelPausedGame
            // 
            _labelPausedGame.AutoSize = true;
            _labelPausedGame.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            _labelPausedGame.Location = new Point(950, 212);
            _labelPausedGame.Name = "_labelPausedGame";
            _labelPausedGame.Size = new Size(199, 32);
            _labelPausedGame.TabIndex = 15;
            _labelPausedGame.Text = "Game is Paused.";
            _labelPausedGame.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // GameForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            ClientSize = new Size(875, 379);
            Controls.Add(_labelPausedGame);
            Controls.Add(_size21);
            Controls.Add(_size15);
            Controls.Add(_size11);
            Controls.Add(_welcomeLabel);
            Controls.Add(_buttonStepRight);
            Controls.Add(_buttonStepLeft);
            Controls.Add(_buttonStepDown);
            Controls.Add(_buttonStepUp);
            Controls.Add(_pauseButton);
            Controls.Add(statusStrip1);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "GameForm";
            Text = "Escape Game";
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private StatusStrip statusStrip1;
        private ToolStripStatusLabel _toolLabelGameTime;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem _menuNewGame;
        private ToolStripMenuItem _menuLoadGame;
        private ToolStripMenuItem _menuSaveGame;
        private ToolStripMenuItem _menuExitGame;
        private OpenFileDialog _openFileDialog;
        private SaveFileDialog _saveFileDialog;
        private Button _pauseButton;
        private Button _buttonStepRight;
        private Button _buttonStepLeft;
        private Button _buttonStepDown;
        private Button _buttonStepUp;
        private Label _welcomeLabel;
        private Button _size11;
        private Button _size15;
        private Button _size21;
        private Label _labelPausedGame;
        private ToolStripMenuItem _menuSizeChange;
    }
}