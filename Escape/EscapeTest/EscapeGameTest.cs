using Escape.Model;
using Escape.Persistence;
using Moq;
using System.Drawing;

namespace EscapeTest
{
    [TestClass]
    public class EscapeGameModelTests : IDisposable
    {
        #region Fields
        //creating test fields
        private EscapeGameModel _model = null!;
        private EscapeTable _mockedTable = null!;
        private Mock<IEscapeDataAccess> _mock = null!;
        #endregion

        #region Constructor
        [TestInitialize]
        public void Initialize()
        {
            //initializing the tests with an 11x11 sized Escapetable and adding events
            _mock = new Mock<IEscapeDataAccess>();
            _mock.Setup(mock => mock.LoadAsync(It.IsAny<String>()))
                .Returns(() => Task.FromResult(_mockedTable));

            _model = new EscapeGameModel(TableSize.ELEVEN, _mock.Object);

            _model.GameAdvanced += new EventHandler<EscapeEventArgs>(Model_AdvancedGame);
            _model.GameOver += new EventHandler<EscapeEventArgs>(Model_GameOver);
        }
        #endregion

        #region Test Methods
        [TestMethod]
        public void EscapeGameModelNewGameElevenTest()
        {
            _model = new EscapeGameModel(TableSize.ELEVEN, _mock.Object); //making a new model
            _model.NewGame(); //starting a new game 

            Assert.AreEqual(TableSize.ELEVEN, _model.tableSize); //the model is 11x11 large
            Assert.AreEqual(11, _model.Table.Size); // its size is 11 
            Assert.AreEqual(new Point(0, _model.Table.Size / 2), _model.Table.PlayerCoordinates); //player has spawned at the middle of the top row of the table
            Assert.AreEqual(new Point(_model.Table.Size - 1, 0), _model.Table.Enemy1Coordinates); //the two enemies have spawned at the to edges of the bottom row
            Assert.AreEqual(new Point(_model.Table.Size - 1, _model.Table.Size - 1), _model.Table.Enemy2Coordinates);
            Assert.AreEqual(6, _model.Table.MinesCount); //there have been 6 mines spawned on the table
            Int32 emptyFields = 0;
            for (Int32 i = 0; i < _model.Table.Size; i++)
                for (Int32 j = 0; j < _model.Table.Size; j++)
                    if (_model.Table.IsEmpty(i, j))
                        emptyFields++;
            Assert.AreEqual(112, emptyFields); // the remaining fields are empty: their number is 112 (11*11 - (1 + 2 + 6))
        }

        [TestMethod]
        public void EscapeGameModelNewGameFifteenTest() //same tests happen like in the previous testmethod
        {
            _model = new EscapeGameModel(TableSize.FIFTEEN, _mock.Object);
            _model.NewGame();

            Assert.AreEqual(TableSize.FIFTEEN, _model.tableSize);
            Assert.AreEqual(15, _model.Table.Size);
            Assert.AreEqual(new Point(0, _model.Table.Size / 2), _model.Table.PlayerCoordinates);
            Assert.AreEqual(new Point(_model.Table.Size - 1, 0), _model.Table.Enemy1Coordinates);
            Assert.AreEqual(new Point(_model.Table.Size - 1, _model.Table.Size - 1), _model.Table.Enemy2Coordinates);
            Assert.AreEqual(6, _model.Table.MinesCount);
            Int32 emptyFields = 0;
            for (Int32 i = 0; i < _model.Table.Size; i++)
                for (Int32 j = 0; j < _model.Table.Size; j++)
                    if (_model.Table.IsEmpty(i, j))
                        emptyFields++;
            Assert.AreEqual(15*15 - 9, emptyFields);
        }

        [TestMethod]
        public void EscapeGameModelNewGameTwentyoneTest() //same tests happen like in the previous testmethod
        {
            _model = new EscapeGameModel(TableSize.TWENTYONE, _mock.Object);
            _model.NewGame();

            Assert.AreEqual(TableSize.TWENTYONE, _model.tableSize);
            Assert.AreEqual(21, _model.Table.Size);
            Assert.AreEqual(new Point(0, _model.Table.Size / 2), _model.Table.PlayerCoordinates);
            Assert.AreEqual(new Point(_model.Table.Size - 1, 0), _model.Table.Enemy1Coordinates);
            Assert.AreEqual(new Point(_model.Table.Size - 1, _model.Table.Size - 1), _model.Table.Enemy2Coordinates);
            Assert.AreEqual(6, _model.Table.MinesCount);
            Int32 emptyFields = 0;
            for (Int32 i = 0; i < _model.Table.Size; i++)
                for (Int32 j = 0; j < _model.Table.Size; j++)
                    if (_model.Table.IsEmpty(i, j))
                        emptyFields++;
            Assert.AreEqual(21*21 - 9, emptyFields);
        }

        [TestMethod]
        public void EscapeGameModelStepTest()
        {
            _model.NewGame();
            //default table size is 11x11
            Assert.AreEqual(new Point(0,5), _model.Table.PlayerCoordinates); //spawning player on the appropriate field
            _model.Step(new Point(5,5)); // stepping five right (player is at the right top corner of the table)
            Assert.AreEqual(new Point(0 + 5, 5 + 5), _model.Table.PlayerCoordinates);
            _model.Step(new Point(0, 1)); //attempt to step one more right
            Assert.AreEqual(new Point(0 + 5, 5 + 5), _model.Table.PlayerCoordinates); //doesn't step out of the table


        }

        [TestMethod]
        public void EscapeGameModelSteppingEnemiesTest()
        {
            _model.NewGame();
            //default table size is 11x11
            Assert.AreEqual(new Point(10, 0), _model.Table.Enemy1Coordinates); //enemies spawn at the two bottom corners of the table
            Assert.AreEqual(new Point(10, 10), _model.Table.Enemy2Coordinates);
            //a timer controls the steps of the enemies, if the time is elapsing, enemies will step one per a second
            _model.SteppingEnemy();
            

            //they tried to go closer to the player (player is on the top,so enemies go upper)
            Assert.AreEqual(new Point(9, 0), _model.Table.Enemy1Coordinates);
            Assert.AreEqual(new Point(9, 10), _model.Table.Enemy2Coordinates); 
        }

        [TestMethod]
        public async Task EscapeGameModelLoadTest() //loading a game
        {
            _model.NewGame();
            _mockedTable = _model.Table;

            await _model.LoadGameAsync(String.Empty); 

            //after loading, we check if every character is on the right place
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Assert.AreEqual(_mockedTable.GetValue(i,j), _model.Table.GetValue(i,j));
                    Assert.AreEqual(_mockedTable.IsMine(i, j), _model.Table.IsMine(i, j));
                }
            }
        }
        
        [TestMethod]
        public void GameOverTest_EnemiesCatchPlayer() //shows the case when an enemy catches the player
        {
            _model.Table.SetPlayer(10, 10);
            _model.Table.SetEnemy(9, 10); //this enemy is one step from the player
            _model.Table.SetEnemy(0, 0);
            _model.Table.SetEnemyCoordinate(1, new Point(9, 10));
            _model.Table.SetEnemyCoordinate(2, new Point(0,0));
            _model.SteppingEnemy(); //enemies step one field closer to the player, so enemy1 catches the player
            //Model_GameOver is called
        }
        
        [TestMethod]
        public void GameOverTest_PlayerFallsIntoAMine()
        {
            _model.Table.SetPlayer(0, 0);
            _model.Table.SetEnemy(10,10);
            _model.Table.SetMine(0, 1); //setting mines around the player
            _model.Table.SetMine(1, 0);
            _model.Step(new Point(0,1)); //player falls into a mine
            //Model_GameOver is called
        }

        [TestMethod]
        public void GameAdvanceTest()
        {
            _model.NewGame();
            _model.SteppingEnemy(); //steps of enemies and gametime are connected, so enemies have to take a step to increase the gametime by one
            _model.AdvanceGame(); // the method which calls the GameAdvanced event 
            //Model_AdvancedGame is called
        }

        //two events of the fake model.
        private void Model_AdvancedGame(Object? sender, EscapeEventArgs e)
        {
            Assert.IsTrue(_model.GameTime > 0); //gametime is more than zero
        }
        private void Model_GameOver(Object? sender, EscapeEventArgs e)
        {
            Assert.IsTrue(_model.IsGameOver); // the game is surely over
            Assert.IsFalse(e.IsWon); //enemies won, we lost
        }

        public void Dispose()
        {
            (_model as IDisposable)?.Dispose();
        }

        [TestCleanup]
        public void Cleanup()
        {
            if (_model != null)
            {
                (_model as IDisposable)?.Dispose();
            }
        }
        #endregion
    }
}